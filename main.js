import {CofActor} from "/systems/cof/module/actors/actor.js";

Hooks.on("init",async function(){
    CONFIG.Actor.entityClass = CustomCofActor;
});

export class CustomCofActor extends CofActor{
    
    // Déplacement des méthodes "computeModsAndAttributes" et "computeAttacks" vers prepareDerivedCharacterData
    // Pour qu'elles s'éxécutent APRES les effets et qu'elles aient accès aux modificateurs
    _prepareBaseCharacterData(actorData) { }

    _prepareDerivedCharacterData(actorData) {
        this.computeModsAndAttributes(actorData);
        this.computeAttacks(actorData);
        
        super._prepareDerivedCharacterData(actorData);
    }
}