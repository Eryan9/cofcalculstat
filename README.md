# Chroniques Oubliées Fantasy - Correction calcul stats #  
  
Ce module corrige la prise en compte des modificateurs 'Bonus' provenant des effets.  
Ainsi, dans les effets, au lieu de devoir modifier "bonus", "value" et "mod", il ne faut plus modifier que "bonus".  
Le bonus sera pris en compte pour le calcul de la valeur et du mod.  
ex :  
Avant : Pour ajouter +1 au mod FOR, il fallait  
- data.stats.str.bonus +2  
- data.stats.str.value +2  
- data.stats.str.mod +1  
  
Après :  
- data.stats.str.bonus +2  
  
Les autres valeurs seront recalculées automatiquement  

